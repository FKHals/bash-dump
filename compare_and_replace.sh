#!/bin/bash
# Program to visually compare and possibly replace one directory structure with another
# file by file.
# Written by Felix Korthals
# June 2020

# Use-case: If you have two very similiar programs (e. g. e newer and an older version)
# and you want to merge a lot of code from one to the other (update the old code with the new)
# but you want to do the changes one file at a time but you may not want to change every
# file for some reason (e. g. some compatibility)

# new directory that overwrites
NEW_DIR=/some/new/directory
# old directory that (probably) gets overwritten
OLD_DIR=/some/old/directory

# regex to display only files with a certain path (that matches the expression)
# example: "*items*" if you only want files with the substring "items" in the path
RE_PATH="*certain_path/*"
# regex to display only files with a certain extension (that matches the expression)
# example: ".java"
RE_EXTENSION="*.certain_extension"

# find all files (relative paths) with a certain path and extension
FILENAMES=$(find $NEW_DIR -path $RE_PATH -type f -name $RE_EXTENSION -exec realpath --relative-to $NEW_DIR {} \;)

for NAME in $FILENAMES
do
    # display the filepath
    echo $NEW_DIR/$NAME
    # exclude space-like characters and the Copyright-Statement from the diff (since
    # changing dates do not matter) and make it silent
    DIFF=$(colordiff -rbB -I "^.*Copyright.*" $OLD_DIR/$NAME $NEW_DIR/$NAME 2> /dev/null)
    # if there is a difference or the file does not exist in the compared directory
    if [[ $(echo $DIFF | tr -d '\n') != "" ]] || [ ! -f "$OLD_DIR/$NAME" ];
    then
        # display the previously generated difference
        echo "$DIFF"
        input_invalid=true
        # ask again, if the input was not valid [y(es), n(o), q(uit)]
        while [ "$input_invalid" = true ]
        do
            read -n 1 -p "Press [y] to replace, [n] to NOT replace and [q] to abort" reply
            case $reply in
                [yY])
                    printf "\nreplace\n"
                    # overwrite the old file with the content of the new one
                    cat $NEW_DIR/$NAME > $OLD_DIR/$NAME
                    input_invalid=false
                    ;;
                [nN])
                    printf "\ndon't replace\n"
                    input_invalid=false
                    ;;
                [qQ])
                    printf "\nexit\n"
                    return 0
                    ;;
                *)
                    printf "\ninvalid input\n"
                    ;;
            esac
        done
    fi
done
