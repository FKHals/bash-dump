#!/bin/bash
# This Script recursively extracts nested ZIP files from the local directory while maintaining the original structure
# Written by Felix Korthals
# March 2020

# run the loop until no .zip files are found in the local directory structure
while [ "`find . -type f -name '*.zip' | wc -l`" -gt 0 ]; do
    # loop over all folders that contain ZIP files
    for DIR in $(find . -name "*.zip" | while read x; do echo $(dirname $x); done | sort -u)
    do
        # extract all ZIP files in the found directory
        find $DIR -name "*.zip" | xargs -P 5 -I fileName sh -c 'unzip -o -d "$(dirname "fileName")/$(basename -s .zip "fileName")" "fileName"'
        # remove all ZIP files after extraction
        rm $DIR/*.zip
        # move subdirectories up if they have parents of the same name (name/name/ -> name/) and do it silently because its just trial and error
        find $DIR -maxdepth 1 | while read directory; do dir=$(basename $directory); mv $DIR/$dir/$dir/* $DIR/$dir/; rmdir $DIR/$dir/$dir/; done 2> /dev/null
    done
done
