#!/bin/bash
# Program to merge all PDFs in the working directory together into a single PDF that
# contains all other PDFs in sorted order
# Written by Felix Korthals
# July 2020

# partial source: https://stackoverflow.com/questions/2507766/merge-convert-multiple-pdf-files-into-one-pdf/19358402#19358402

# PDF optimization options
# -dPDFSETTINGS=/screen   (screen-view-only quality, 72 dpi images)
# -dPDFSETTINGS=/ebook    (low quality, 150 dpi images)
# -dPDFSETTINGS=/printer  (high quality, 300 dpi images)
# -dPDFSETTINGS=/prepress (high quality, color preserving, 300 dpi imgs)
# -dPDFSETTINGS=/default  (almost identical to /screen)

MERGED_FILENAME=merged
OPTIMIZATION=/ebook

find . -type f -name "*.pdf" -print0 | sort -z | xargs -r0 gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=$OPTIMIZATION -sOutputFile=$MERGED_FILENAME.pdf
