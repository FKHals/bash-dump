#!/bin/bash
# This one-liner replaces all occurances of a certain string with a different
# string in all files of the current directory (.) and all subdirectories.

# Use grep flag `--exclude-dir=excluded_directory` to exclude a directory
# (named "excluded_directory").
# Use the `I`-flag of grep `grep -Irl` to avoid checking binaries.
grep -rl oldtext . | xargs sed -i 's/oldtext/newtext/g'
