# Start your scripts with these lines if you don't want your bash scripts to fail
# silently (which is really hard to debug) but instead to show the failed command and
# it's line number.
# Sources:
# https://disconnected.systems/blog/another-bash-strict-mode/
# http://redsymbol.net/articles/unofficial-bash-strict-mode/

#!/bin/bash

# use an unofficial "strict-mode" so the script won't fail silently
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
# set Internal Field Separtor to make word splittings only happen on newlines or tabs
# because the (normally present) whitespaces are "likely to cause surprising and
# confusing bugs" (see http://redsymbol.net/articles/unofficial-bash-strict-mode/)
IFS=$'\n\t'

