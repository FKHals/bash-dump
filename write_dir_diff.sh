#!/bin/bash
# Program to generate and write diffs for all files in all the subdirectories of a directory
# Written by Felix Korthals
# June 2020

# name of the directory where the diffs get stored
DIFFDIR=diffs

# new directory to compare
NEW_DIR=/some/new/directory
# old directory to compare
OLD_DIR=/some/old/directory

# create directory for the diffs if not existing
mkdir -p $DIFFDIR

for NAME in $(ls $NEW_DIR)
do 
    # exclude space-like characters and the Copyright-Statement from the diff (since
    # changing dates do not matter)
    diff -rbB -I "^.*Copyright.*" $OLD_DIR/$NAME $NEW_DIR/$NAME > $DIFFDIR/$NAME.diff
done
